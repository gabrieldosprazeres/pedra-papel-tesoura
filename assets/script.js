const elementos = document.querySelectorAll('.player-moves div img');
let playerMove = "";
let enemyMove = "";

const validarVitoria = () => {
    let vencedor = document.querySelector('.vencedor');

    if (playerMove == "papel") {
        if (enemyMove == "papel") {
            vencedor.innerHTML = "Ahhh, deu empate!";
        } else if (enemyMove == "tesoura") {
            vencedor.innerHTML = "O adversário ganhou!!";
        } else if (enemyMove == "pedra") {
            vencedor.innerHTML = "Você ganhou!!";
        }
    }

    if (playerMove == "tesoura") {
        if (enemyMove == "papel") {
            vencedor.innerHTML = "Você ganhou!!";
        } else if (enemyMove == "tesoura") {
            vencedor.innerHTML = "Ahhh, deu empate!";
        } else if (enemyMove == "pedra") {
            vencedor.innerHTML = "O adversário ganhou!!";
        }
    }

    if (playerMove == "pedra") {
        if (enemyMove == "papel") {
            vencedor.innerHTML = "O adversário ganhou!!";
        } else if (enemyMove == "tesoura") {
            vencedor.innerHTML = "Você ganhou!!";
        } else if (enemyMove == "pedra") {
            vencedor.innerHTML = "Ahhh, deu empate!";
        }
    }
}

const resetInimigo = () => {
    const enemyMoves = document.querySelectorAll('.enemy-moves div');
    for (let i = 0; i < enemyMoves.length; i++) {
        enemyMoves[i].childNodes[0].style.opacity = 0.3;
    }
}

const inimigoJogar = () => {
    let rand = Math.floor(Math.random() * 3);
    const enemyMoves = document.querySelectorAll('.enemy-moves div');
    resetInimigo();
    for (let i = 0; i < enemyMoves.length; i++) {
        if (i == rand) {
            enemyMoves[i].childNodes[0].style.opacity = 1;
            enemyMove = enemyMoves[i].childNodes[0].getAttribute('play');
        }
    }
    validarVitoria();
}

const resetOpacityPlayer = () => {
    for (let i = 0; i < elementos.length; i++) {
        elementos[i].style.opacity = 0.3;
    }
}

for (let i = 0; i < elementos.length; i++) {
    elementos[i].addEventListener('click', function (t) {
        resetOpacityPlayer();
        t.target.style.opacity = 1;
        playerMove = t.target.getAttribute('play');
        inimigoJogar();
    });
}